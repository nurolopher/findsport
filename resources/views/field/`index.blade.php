@extends('layouts.master')
@section('title','Page Title')
@section('sidebar')
    @parent
    <p>This is appended to the master sidebar.</p>
@endsection
@section('content')
    @foreach($fields as $field)
        <div>
            <div>
                <a href="{{action('FieldController@show',['id'=>$field->id])}}">{{$field->name}}</a>
            </div>
        </div>
    @endforeach
@endsection