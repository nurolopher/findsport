<!doctype html>
<html lang="en" ng-app="app">
<head>
    <meta charset="UTF-8">
    <title>{{'TITLE'|translate}}</title>
    <!-- CSS -->
    <link rel="stylesheet" href="css/app.css">
    <link rel="stylesheet" href="css/all.css">
    <!--[if lte IE 9]>
    <script src="//cdnjs.cloudflare.com/ajax/libs/Base64/0.3.0/base64.min.js"></script>
    <![endif]-->
</head>
<!-- declare our angular app and controller -->
<!--ng-controller="FieldController"-->
<body>

<nav class="navbar navbar-default ">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                    aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#/fields">
                <img src="http://www.sotolabs.com/img/angularjs.png" alt="Brand">
            </a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li><a href="#about"><strong>{{'ABOUT'| translate}}</strong></a></li>
                <li><a href="#contact"><strong>{{'CONTACT'|translate}}</strong></a></li>
            </ul>
            <div class="nav navbar-form navbar-right" ng-controller="userCtrl">
                <div ng-hide="authenticated"><a href="#sign_up">
                        <button class="btn btn-success">{{'SIGN_UP' |translate}}</button>
                    </a>
                    <a href="#login">
                        <button class="btn btn-default">{{'SIGN_IN'| translate}}</button>
                    </a></div>
                <ul class="nav" ng-show="authenticated">
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="">
                            {{'PROFILE' |translate}} <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li><a href="#logout" ng-click="logout()">{{'LOGOUT'|translate}}</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</nav>
<div class="container">
    <div class="alert alert-success alert-dismissible" role="alert" ng-repeat="message in messages">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                aria-hidden="true">&times;</span>
        </button>
        {{message}}
    </div>
</div>
<div class="container" autoscroll="true" ng-view></div>
<!-- JS -->
<script src="js/all.js"></script>
<!-- load angular -->

<!-- ANGULAR -->
<script src="js/angular/controllers/fieldsController.js"></script>
<script src="js/angular/controllers/fieldDetailsController.js"></script>
<script src="js/angular/controllers/mapController.js"></script>
<script src="js/angular/controllers/ReservationsController.js"></script>
<script src="js/angular/controllers/authController.js"></script>
<script src="js/angular/controllers/userCtrl.js"></script>
<script src="js/angular/services/fieldService.js"></script>
<script src="js/angular/app.js"></script>

<script src="js/angular/filters/filters.js"></script>
<script src="js/angular/directives/directives.js"></script>
<script src="js/angular/translations/translations.js"></script>
<script src="js/angular/routes/routes.js"></script>
<!--<script src="https://maps.googleapis.com/maps/api/js"></script>-->
</body>
</html>