var elixir = require('laravel-elixir');
var gulp = require('gulp');
/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Less
 | file for our application, as well as publishing vendor resources.
 |
 */

gulp.task("copyfiles", function () {
    gulp.src("bower_components/jquery/dist/jquery.min.js")
        .pipe(gulp.dest("resources/assets/js/"));
    gulp.src("bower_components/angular/angular.min.js")
        .pipe(gulp.dest("resources/assets/js/"));
    gulp.src("bower_components/angular-route/angular-route.min.js")
        .pipe(gulp.dest("resources/assets/js/"));
    gulp.src("bower_components/angularjs-slider/dist/rzslider.min.js")
        .pipe(gulp.dest("resources/assets/js/"));
    gulp.src("bower_components/checklist-model/checklist-model.js")
        .pipe(gulp.dest("resources/assets/js/"));

    gulp.src("bower_components/angular-bootstrap/ui-bootstrap-tpls.min.js")
        .pipe(gulp.dest("resources/assets/js/"));

    gulp.src("bower_components/bootstrap-sass-official/assets/javascripts/bootstrap.min.js")
        .pipe(gulp.dest("resources/assets/js/"));

    /*Bootstrap*/
    gulp.src("bower_components/bootstrap-sass-official/assets/stylesheets/bootstrap/**")
        .pipe(gulp.dest("resources/assets/sass/bootstrap"));
    gulp.src("bower_components/bootstrap-sass-official/assets/stylesheets/_bootstrap.scss")
        .pipe(gulp.dest("resources/assets/sass/"));
    gulp.src("bower_components/bootstrap-sass-official/assets/fonts/**")
        .pipe(gulp.dest("public/fonts/"));

    /*angularjs slider*/
    gulp.src("bower_components/angularjs-slider/dist/rzslider.min.css")
        .pipe(gulp.dest("resources/assets/css/"));

    /*justified gallery copy*/
    gulp.src("bower_components/justified-gallery/dist/js/jquery.justifiedGallery.min.js")
        .pipe(gulp.dest("resources/assets/js/"));
    gulp.src("bower_components/justified-gallery/dist/css/justifiedGallery.min.css")
        .pipe(gulp.dest("resources/assets/css/"));

    /*Color box slider*/
    gulp.src("bower_components/jquery-colorbox/jquery.colorbox-min.js")
        .pipe(gulp.dest("resources/assets/js/"));
    gulp.src("bower_components/jquery-colorbox/example1/colorbox.css")
        .pipe(gulp.dest("resources/assets/css/"));

    gulp.src("bower_components/jquery-colorbox/example1/images/**")
        .pipe(gulp.dest("public/css/images/"));

    /*Font Awesome*/
    gulp.src("bower_components/font-awesome/scss/**")
        .pipe(gulp.dest("resources/assets/sass/font-awesome/scss"));
    gulp.src("bower_components/font-awesome/fonts/**")
        .pipe(gulp.dest("public/fonts/"));

    /*Google Maps Angular*/
    gulp.src("bower_components/angular-google-maps/dist/angular-google-maps.min.js")
        .pipe(gulp.dest("resources/assets/js/"));

    gulp.src("bower_components/lodash/lodash.min.js")
        .pipe(gulp.dest("resources/assets/js/"));

    gulp.src("bower_components/moment/moment.js")
        .pipe(gulp.dest("resources/assets/js/"));

    gulp.src("bower_components/angular-translate/angular-translate.min.js")
        .pipe(gulp.dest("resources/assets/js/"));

    //satellizer
    gulp.src("bower_components/satellizer/satellizer.min.js")
        .pipe(gulp.dest("resources/assets/js/"));

    gulp.src("bower_components/angular-sanitize/angular-sanitize.min.js")
        .pipe(gulp.dest("resources/assets/js"));
});

elixir(function (mix) {
    mix.sass('app.scss');
    mix.styles([
        "rzslider.min.css",
        "justifiedGallery.min.css",
        "colorbox.css"
    ]);
    mix.scripts(["jquery.min.js",
            "moment.js",
            "bootstrap.min.js",
            "angular.min.js",
            "angular-route.min.js",
            "rzslider.min.js",
            "checklist-model.js",
            "ui-bootstrap-tpls.min.js",
            "jquery.justifiedGallery.min.js",
            "jquery.colorbox-min.js",
            "lodash.min.js",
            "angular-google-maps.min.js",
            "angular-translate.min.js",
            "satellizer.min.js",
            "angular-sanitize.min.js"
        ]
    );

});