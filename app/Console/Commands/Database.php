<?php
/**
 * Created by PhpStorm.
 * User: nurolopher
 * Date: 8/15/2015
 * Time: 4:35 PM
 */

namespace App\Console\Commands;


use Illuminate\Console\Command;

class Database extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'd:recreate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Drop database and create.';

    /**
     * Create a new command instance.
     *
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        shell_exec("mysql -uroot -proot; drop");
    }
}