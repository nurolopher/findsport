<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class Thumbnail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'thumbnail';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generates thumbnails.';

    /**
     * Create a new command instance.
     *
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        shell_exec("cd ./public/uploads ; rm -r ./fields_thumbnail ; mkdir ./fields_thumbnail ; cp ./fields/* ./fields_thumbnail ; cd ./fields_thumbnail ; mogrify -resize 160x120 *.jpg");
    }
}
