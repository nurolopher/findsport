<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Photo
 *
 * @property integer $id
 * @property string $url
 * @property integer $field_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read Field $field
 * @method static \Illuminate\Database\Query\Builder|\App\Photo whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Photo whereUrl($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Photo whereFieldId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Photo whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Photo whereUpdatedAt($value)
 */
class Photo extends Model
{
    public $location = './uploads/fields/';
    public function field()
    {
        return $this->belongsTo(Field::class);
    }
}
