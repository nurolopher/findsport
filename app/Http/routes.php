<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::group(['prefix' => 'api'], function () {

    Route::post('login', 'AuthenticateController@login');
    Route::post('signUp', 'AuthenticateController@signUp');

    Route::resource('field', 'FieldController',
        ['only' => ['index', 'show']]);
    Route::resource('cover', 'CoverController',
        ['only' => ['index']]);
    Route::resource('infrastructure', 'InfrastructureController',
        ['only' => ['index']]);
    Route::get('review/statistics', 'ReviewController@statistics');

    Route::resource('category', 'CategoryController',
        ['only' => ['index']]);

    Route::resource('map', 'MapController',
        ['only' => ['index', 'show']]);

    Route::resource('reservation', 'ReservationController',
        ['except' => []]);

    Route::group(['prefix' => 'field'], function () {
        Route::get('{id}/reservations', 'ReservationController@fieldReservations');
        Route::post('{id}/reservations/store', 'ReservationController@storeReservations');
    });
});
