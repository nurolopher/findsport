<?php

namespace App\Http\Controllers;

use App\Field;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;

class FieldController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $fields = Field::with('company', 'infrastructures', 'photos', 'reviews', 'category')->get();
        return response()->json($fields);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        $field = Field::with('photos', 'company', 'reviews.user')->where('id', '=', $id)->get();
        return response()->json($field);
    }
}
