<?php

namespace App\Http\Controllers;

use App\Review;
use DB;

use App\Http\Requests;
use Symfony\Component\HttpFoundation\Response;

class ReviewController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $reviews = Review::all();
        return response()->json($reviews);
    }

    public function statistics()
    {
        $reviews = DB::table('reviews')
            ->groupBy('field_id')
            ->select(DB::raw('field_id, count(field_id) as number, round((avg(rating_location)+avg(rating_service)+avg(rating_service))/3,2) as average'))
            ->get();
        return response()->json($reviews);
    }
}
