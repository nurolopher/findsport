<?php

namespace App\Http\Controllers;

use App\Category;

use App\Http\Requests;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return response()->json(Category::all());
    }
}
