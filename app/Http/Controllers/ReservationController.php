<?php

namespace App\Http\Controllers;

use App\Field;
use App\Reservation;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ReservationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }


    public function fieldReservations($id)
    {
        $field = Field::with('company', 'reservations')->find($id);
        return response()->json($field);
    }

    /**
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\JsonResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function storeReservations(Request $request, $id)
    {
        $hours = $request->json()->all();
        try{
            foreach ($hours as $hour) {
                $reservation = new Reservation();
                $reservation->starts_at = new \DateTime($hour.":00");
                $reservation->field_id = $id;
                $reservation->save();
            }
        }catch (\Exception $e){
            return response($e->getMessage(),501);
        }
        return response()->json("success");
    }
}
