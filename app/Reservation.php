<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Reservation
 *
 * @property integer $id 
 * @property string $starts_at 
 * @property integer $field_id 
 * @property \Carbon\Carbon $created_at 
 * @property \Carbon\Carbon $updated_at 
 * @property-read Field $field 
 * @method static \Illuminate\Database\Query\Builder|\App\Reservation whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Reservation whereStartsAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Reservation whereFieldId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Reservation whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Reservation whereUpdatedAt($value)
 */
class Reservation extends Model
{
    public function field()
    {
        return $this->belongsTo(Field::class);
    }
}
