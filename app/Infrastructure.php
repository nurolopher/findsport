<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Infrastructure
 *
 * @property integer $id
 * @property string $name
 * @property-read \Illuminate\Database\Eloquent\Collection|Field[] $fields
 * @method static \Illuminate\Database\Query\Builder|\App\Infrastructure whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Infrastructure whereName($value)
 */
class Infrastructure extends Model
{
    public $timestamps = false;

    public function fields(){
        return $this->hasMany(Field::class);
    }
}
