<?php

namespace App;

use DB;
use SleepingOwl\Models\Interfaces\ModelWithImageFieldsInterface;
use SleepingOwl\Models\SleepingOwlModel;

/**
 * App\Field
 *
 * @property integer $id
 * @property string $name
 * @property string $opens
 * @property string $closes
 * @property boolean $roof
 * @property float $price
 * @property string $phoneNumber
 * @property string $address
 * @property string $description
 * @property float $width
 * @property float $height
 * @property integer $maximumPlayers
 * @property float $longitude
 * @property float $latitude
 * @property integer $category_id
 * @property integer $cover_id
 * @property integer $city_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read mixed $average
 * @property-read mixed $map
 * @property-read \Illuminate\Database\Eloquent\Collection|Infrastructure[] $infrastructures
 * @property-read \Illuminate\Database\Eloquent\Collection|Review[] $reviews
 * @property-read Category $category
 * @property-read Cover $cover
 * @property-read \Illuminate\Database\Eloquent\Collection|Photo[] $photos
 * @method static \Illuminate\Database\Query\Builder|\App\Field whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Field whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Field whereOpens($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Field whereCloses($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Field whereRoof($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Field wherePrice($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Field wherePhoneNumber($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Field whereAddress($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Field whereDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Field whereWidth($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Field whereHeight($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Field whereMaximumPlayers($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Field whereLongitude($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Field whereLatitude($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Field whereCategoryId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Field whereCoverId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Field whereCityId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Field whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Field whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\SleepingOwl\Models\SleepingOwlModel defaultSort()
 * @property integer $maximum_players 
 * @property integer $company_id 
 * @property-read Company $company 
 * @property-read \Illuminate\Database\Eloquent\Collection|Reservation[] $reservations 
 * @method static \Illuminate\Database\Query\Builder|\App\Field whereCompanyId($value)
 */
class Field extends SleepingOwlModel implements ModelWithImageFieldsInterface
{
    protected $casts = [
        'price' => 'float'
    ];

    protected $appends = [
        'average',
        'map'
    ];

    public function getAverageAttribute()
    {
        return $this->reviews()->select(DB::raw('round(avg( cast(rating_location as numeric)),2) as location, round(avg(cast(rating_service as numeric)),2) as service, round(avg(rating_condition),2) as condition'))->first();
    }

    public function getMapAttribute()
    {
        return array(
            'id' => $this->id,
            'coords' => array(
                'latitude' => $this->latitude,
                'longitude' => $this->longitude
            ),
            'options' => array(
                'draggable' => false,
            ),
            'zoom' => 8
        );
    }

    public function infrastructures()
    {
        return $this->belongsToMany(Infrastructure::class);
    }

    public function reviews()
    {
        return $this->hasMany(Review::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function cover()
    {
        return $this->hasOne(Cover::class);
    }

    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    public function reservations()
    {
        return $this->hasMany(Reservation::class);
    }

    public function photos()
    {
        return $this->hasMany(Photo::class);
    }

    /**
     * Get array of image field names and its directories within images folder
     *
     * Keys of array is image field names
     * Values is their directories
     *
     * @return string[]
     */
    public function getImageFields()
    {
        return [
            'image' => 'fields/',
            'photo' => '',
            'other' => [
                'other_images/',
                function ($directory, $originalName, $extension) {
                    return $originalName;
                }
            ]
        ];
    }

    /**
     * @param $field
     * @return bool
     */
    public function hasImageField($field)
    {
        // TODO: Implement hasImageField() method.
    }
}
