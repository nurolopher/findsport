<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Cover
 *
 * @property integer $id
 * @property string $name
 * @property-read \Illuminate\Database\Eloquent\Collection|Field[] $fields
 * @method static \Illuminate\Database\Query\Builder|\App\Cover whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Cover whereName($value)
 */
class Cover extends Model
{
    public $timestamps = false;

    public function fields()
    {
        return $this->hasMany(Field::class);
    }
}
