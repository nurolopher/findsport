<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Company
 *
 * @property integer $id 
 * @property string $name 
 * @property string $description 
 * @property string $phone_number 
 * @property string $address 
 * @property string $opens_at 
 * @property string $closes_at 
 * @property integer $time_split 
 * @property float $longitude 
 * @property float $latitude 
 * @property integer $city_id 
 * @property \Carbon\Carbon $created_at 
 * @property \Carbon\Carbon $updated_at 
 * @property-read \Illuminate\Database\Eloquent\Collection|Field[] $fields 
 * @method static \Illuminate\Database\Query\Builder|\App\Company whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Company whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Company whereDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Company wherePhoneNumber($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Company whereAddress($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Company whereOpensAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Company whereClosesAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Company whereTimeSplit($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Company whereLongitude($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Company whereLatitude($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Company whereCityId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Company whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Company whereUpdatedAt($value)
 */
class Company extends Model
{
    public function fields()
    {
        return $this->hasMany(Field::class);
    }
}
