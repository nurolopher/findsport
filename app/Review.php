<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class  Review extends Model
{
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function field()
    {
        return $this->belongsTo(Field::class);
    }

    /**
     * @inheritDoc
     */
    public function toArray()
    {
        $array = parent::toArray();
        $array['average'] = round(($this->rating_location + $this->rating_condition + $this->rating_condition) / 3, 2);
        return $array;
    }


}
