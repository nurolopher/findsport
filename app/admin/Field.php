<?php
/**
 * Created by PhpStorm.
 * User: nursultan
 * Date: 3-Jul 15
 * Time: 22:50
 */

Admin::model(\App\Field::class)
    ->title('Fields')
    ->with('infrastructures','city','category','cover')
    ->columns(function () {
        Column::string('name', 'Name');
        Column::string('opens', 'Opens');
        Column::string('closes', 'Closes');
        Column::string('roof', 'Roof');
        Column::string('price', 'Price');
        Column::string('phoneNumber', 'PhoneNumber');
        Column::string('address', 'Address');
        Column::string('description', 'Description');
        Column::string('longitude', 'Longitude');
        Column::string('latitude', 'Latitude');
    })
    ->form(function () {
        FormItem::text('name');
        FormItem::time('opens');
        FormItem::time('closes');
        FormItem::checkbox('roof','Roof');
        FormItem::textAddon('price')->addon('Сом')->placement('after');
        FormItem::text('phoneNumber');
        FormItem::text('address');
        FormItem::textarea('description');
        FormItem::text('longitude');
        FormItem::text('latitude');
    });