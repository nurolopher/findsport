<?php
/**
 * Created by PhpStorm.
 * User: nursultan
 * Date: 25-Jun 15
 * Time: 22:17
 */

Admin::model(\App\City::class)
    ->title('Cities')
    ->columns(function () {
        Column::string('name', 'Name');
    })
    ->form(function () {
        FormItem::text('name', 'Name');
    });