[![Build Status](https://travis-ci.org/nurolopher/findsport.svg?branch=master)](https://travis-ci.org/nurolopher/findsport)

##Installation

https://nodejs.org/download/

```

$ curl -sS https://getcomposer.org/installer | php

$ php composer.phar install

$ npm install

$ npm install -g bower

$ npm install gulp

$ bower install

$ gulp copyfiles sass default

$ php artisan migrate:refresh

$ chmod 777 ./public

$ mkdir public/uploads

$ mkdir public/uploads/fields

$ mogrify -thumbnail 160x120 *

$ brew install php55-xdebug

```

**Install PHP 7.0 for MAC**

```

curl -s http://php-osx.liip.ch/install.sh | bash -s 7.0

export PATH=/usr/local/php5/bin:$PATH
```

**install checklist-model**

[http://vitalets.github.io/checklist-model/](checklist-model)
