<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFieldsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fields', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->boolean('roof');
            $table->decimal('price', 10, 2);
            $table->text('description');
            $table->float('width');
            $table->float('height');
            $table->smallInteger('maximum_players', false, true);
            $table->unsignedInteger('category_id');
            $table->unsignedInteger('cover_id');
            $table->unsignedInteger('company_id');
            $table->timestamps();

            $table->foreign('category_id')->references('id')->on('categories');
            $table->foreign('cover_id')->references('id')->on('covers');
            $table->foreign('company_id')->references('id')->on('companies');

            $table->index('category_id');
            $table->index('cover_id');
            $table->index('company_id');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('fields');
    }
}
