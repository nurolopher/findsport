<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reviews', function (Blueprint $table) {
            $table->increments('id');
            $table->text('opinion_negative');
            $table->text('opinion_positive');
            $table->unsignedTinyInteger('rating_location');
            $table->unsignedTinyInteger('rating_service');
            $table->unsignedTinyInteger('rating_condition');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('field_id');
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('field_id')->references('id')->on('fields');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('reviews');
    }
}
