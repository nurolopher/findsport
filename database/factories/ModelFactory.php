<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/
/** @var \Illuminate\Database\Eloquent\Factory $factory */
/** @var \Faker\Provider\Base $faker */
$factory->define(App\User::class, function ($faker) {
    return [
        'name' => $faker->unique()->name,
        'email' => $faker->unique()->email,
        'password' => 'password',
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\City::class, function ($faker) {
    return [
        'name' => $faker->city
    ];
});

$factory->define(\App\Company::class, function ($faker) {
    $time_splits = [10, 15, 30, 60];
    return [
        'name' => $faker->unique()->company,
        'description' => $faker->paragraph(15),
        'phone_number' => $faker->phoneNumber,
        'address' => $faker->address,
        'opens_at' => $faker->time,
        'closes_at' => $faker->time,
        'time_split' => $time_splits[$faker->numberBetween(0, 3)],
        'longitude' => $faker->randomFloat(6, 73, 74),
        'latitude' => $faker->randomFloat(6, 41, 42),
        'city_id' => $faker->numberBetween(1, 10),
    ];
});

$factory->define(\App\Field::class, function ($faker) {
    return [
        'name' => $faker->unique()->company,
        'roof' => $faker->boolean,
        'price' => $faker->numberBetween(200, 2500),
        'description' => $faker->paragraph(15),
        'width' => $faker->numberBetween(15, 48),
        'height' => $faker->numberBetween(25, 48),
        'maximum_players' => $faker->numberBetween(6, 24),
        'cover_id' => $faker->numberBetween(1, 10),
        'category_id' => $faker->numberBetween(1, 10),
        'company_id' => $faker->numberBetween(1, 20)
    ];
});

$factory->define(\App\User::class, function ($faker) {
    return [
        'name' => $faker->firstName . ' ' . $faker->lastName,
        'email' => $faker->unique()->email,
        'password' => bcrypt('secret'),
    ];
});

$factory->define(\App\Review::class, function ($faker) {
    return [
        'opinion_positive' => $faker->paragraph(4),
        'opinion_negative' => $faker->paragraph(4),
        'rating_location' => $faker->numberBetween(1, 10),
        'rating_service' => $faker->numberBetween(1, 10),
        'rating_condition' => $faker->numberBetween(1, 10),
        'field_id' => $faker->numberBetween(1, 20)
    ];
});

$factory->define(\App\Infrastructure::class, function ($faker) {
    return [
        'name' => $faker->unique()->word(10)
    ];
});

$factory->define(\App\Category::class, function ($faker) {
    return [
        'name' => $faker->unique()->word(10)
    ];
});

$factory->define(\App\Cover::class, function ($faker) {
    return [
        'name' => $faker->unique()->word(10)];
});

$factory->define(\App\Photo::class, function ($faker) {
    return [
        'url' => $faker->unique()->image('./public/uploads/fields/', 640, 480, 'sports', false)
    ];
});