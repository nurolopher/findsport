<?php

use Illuminate\Database\Seeder;

class InfrastructureTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Infrastructure::class,10)->create();
    }
}
