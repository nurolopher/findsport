<?php

use App\Review;
use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use SleepingOwl\AdminAuth\Database\Seeders\AdministratorsTableSeeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call(CategoryTableSeeder::class);
        $this->call(InfrastructureTableSeeder::class);
        $this->call(CoverTableSeeder::class);
        $this->call(CityTableSeeder::class);
        $this->call(CompanyTableSeeder::class);
        $this->call(FieldTableSeeder::class);
        $this->call(FieldInfrastructureTableSeeder::class);
        $this->call(UserTableSeeder::class);
        $this->call(AdministratorsTableSeeder::class);
        Model::reguard();
    }
}
