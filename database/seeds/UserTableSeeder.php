<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\User::class, 100)->create()
            ->each(function ($u) {
                $u->reviews()->saveMany(factory(\App\Review::class)->times(10)->make());
            });
    }
}
