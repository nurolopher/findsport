<?php

use Illuminate\Database\Seeder;

class FieldInfrastructureTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 1; $i < 20; $i++) {
            for ($j = 1; $j < rand(4,10); $j++) {
                DB::table('field_infrastructure')->insert(array(
                    'field_id' => $i,
                    'infrastructure_id' => $j
                ));
            }
        }

    }
}
