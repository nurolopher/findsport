<?php

use Illuminate\Database\Seeder;

class FieldTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Field::class, 30)->create()
            ->each(function ($f) {
                $f->photos()->saveMany(factory(\App\Photo::class, rand(3,20))->make());
            });
    }
}
