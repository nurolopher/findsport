angular.module('app')
    .directive('justified', ['$timeout', function ($timeout) {
        return {
            restrict: 'AE',
            link: function (scope, el, attrs) {
                scope.$watch('$last', function (n, o) {
                    if (n) {
                        $timeout(function () {
                            jQuery(el[0]).justifiedGallery({
                                maxRowHeight: 200,
                                rowHeight: 200,
                                fixedHeight: true,
                                margins: 3,
                                randomize: true,
                                rel: 'gallery'
                            }).on('jg.complete', function () {
                                jQuery(this).find('a').colorbox({
                                    maxWidth: '100%',
                                    maxHeight: '100%',
                                    opacity: 0.8,
                                    transition: 'elastic',
                                    current: ''
                                });
                            });
                        });
                    }
                });
            }
        };
    }])
    .directive('repeatDone', [function () {
        return {
            restrict: 'A',
            link: function (scope, element, iAttrs) {
                var parentScope = element.parent().scope();
                if (scope.$last) {
                    parentScope.$last = true;
                }
            }
        };
    }]);