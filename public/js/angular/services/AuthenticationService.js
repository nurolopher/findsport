/**
 * @ngdoc service
 * @name Authentication
 * @description
 * _Please update the description and dependencies._
 *
 * */
angular.module('app.auth', [])
    .factory('Authentication', function () {

        var isAuthenticated = false;
        return {
            isAuthenticated: function (state) {
                if (typeof state !== 'undefined') { isAuthenticated = state; }
                return isAuthenticated;
            }
        }

    });

