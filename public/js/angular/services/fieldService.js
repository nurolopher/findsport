/*jslint browser: true*/
/*global $, angular*/
(function () {
    'use strict';
    angular.module('app.fieldService', [])
        .factory('Field', function ($http) {
            return {
                get: function () {
                    return $http.get('/api/field');
                },
                show: function (id) {
                    return $http.get('/api/field/' + id);
                }
            };

        })
        .factory('Cover', function ($http) {
            return {
                get: function () {
                    return $http.get('api/cover');
                }
            };
        })
        .factory('Infrastructure', function ($http) {
            return {
                get: function () {
                    return $http.get('api/infrastructure');
                }
            };
        })
        .factory('Review', function ($http) {
            return {
                statistics: function () {
                    return $http.get('api/review/statistics');
                }
            };
        })
        .factory('Category', function ($http) {
            return {
                get: function () {
                    return $http.get('api/category');
                }
            };
        })
        .factory('Map', function ($http) {
            return {
                get: function () {
                    return $http.get('api/map');
                },
                show: function (id) {
                    return $http.get('api/map/' + id);
                }
            };
        })
        .factory('Reservation', function ($http) {
            return {
                get: function (id) {
                    return $http.get('api/field/' + id + '/reservations');
                },
                store: function (id, reservations) {
                    return $http.post('/api/field/' + id + '/reservations/store', angular.toJson(reservations));
                }
            };
        });
}());