/**
 * Created by nurolopher on 9/13/2015.
 */
(function () {
    angular.module('app')
        .filter('roofFilter', function () {
            return function (fields, value) {
                if (value > 1) {
                    return fields;
                }
                return fields.filter(function (field) {
                    return field.roof == value;
                });
            };
        })
        .filter('rangeFilter', function () {
            return function (items, priceSlider) {
                var filtered = [],
                    min = parseInt(priceSlider.min, 10),
                    max = parseInt(priceSlider.max, 10);
                angular.forEach(items, function (item) {
                    if (item.price >= min && item.price <= max) {
                        filtered.push(item);
                    }
                });
                return filtered;
            };
        })
        .filter('coverFilter', function () {
            return function (items, covers) {
                var filtered = [];
                angular.forEach(items, function (item) {
                    if (covers.indexOf(item.cover_id) !== -1) {
                        filtered.push(item);
                    }
                });
                return filtered;
            };
        })
        .filter('categoryFilter', function () {
            return function (items, categories) {
                var filtered = [];
                angular.forEach(items, function (item) {
                    if (categories.indexOf(item.category_id) !== 1) {
                        filtered.push(item);
                    }
                });
                return filtered;
            };
        })
        .filter('infFilter', function () {
            return function (items, inf) {
                var filtered = [];
                angular.forEach(items, function (item) {
                    var found = false;
                    angular.forEach(item.infrastructures, function (infrastructure) {
                        if (!found) {
                            if (inf.indexOf(infrastructure.id) !== -1) {
                                filtered.push(item);
                                found = true;
                            }
                        }
                    });
                });
                return filtered;
            };
        });
}());