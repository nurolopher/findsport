/**
 * Created by nurolopher on 9/13/2015.
 */
angular.module('app')
    .config(['$translateProvider', function ($translateProvider) {
        $translateProvider.useSanitizeValueStrategy('sanitize');
        $translateProvider.translations('en', {
            'ABOUT': 'About',
            'ADDRESS': 'Address',
            'CATEGORIES': 'Categories',
            'CONDITION': 'Condition',
            'CONTACT': 'Contact',
            'COVER': 'Cover',
            'DETAILS': 'Details',
            'INFRASTRUCTURE': 'Infrastructure',
            'INFRASTRUCTURES': 'Infrastructures',
            'INVALID_CREDENTIALS': 'Invalid Credentials',
            'LESS_FILTERS': 'Less Filters',
            'LOCATION': 'Location',
            'MORE_FILTERS': 'More Filters',
            'NAME': 'Name',
            'NEVER_MIND': 'Never mind',
            'NEXT': 'Next',
            'NO': 'No',
            'OPEN_HOURS': 'Open Hours',
            'POSTED_BY': 'Posted by',
            'PREV': 'Prev',
            'PRICE_LOWEST': 'Price Lowest First',
            'PRICE_HIGHEST': 'Price Highest First',
            'PROFILE': 'Profile',
            'RESERVE_NOW': 'Reserve Now',
            'ROOF': 'Roof',
            'SIGN_IN': 'Sign in',
            'SIGN_UP': 'Sign up',
            'SOM': 'Som',
            'SORT_BY': 'Sort by',
            'TITLE': 'Title',
            'TODAY': 'Today',
            'TOTAL':'Total',
            'YES': 'Yes'
        });
        $translateProvider.preferredLanguage('en');
    }]);