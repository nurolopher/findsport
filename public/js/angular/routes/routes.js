/**
 * Created by nurolopher on 9/13/2015.
 */
angular.module('app')
    .config(['$routeProvider',
        function ($routeProvider) {
            $routeProvider.
                when('/fields', {
                    templateUrl: 'partials/field-list.html',
                    controller: 'FieldsController'
                })
                .when('/fields/:field_id', {
                    templateUrl: 'partials/field-details.html',
                    controller: 'FieldDetailsController'
                })
                .when('/map/:field_id', {
                    templateUrl: 'partials/map.html',
                    controller: 'MapController'
                })
                .when('/field/:field_id/reservations', {
                    templateUrl: 'partials/reservations.html',
                    controller: 'ReservationsController'
                })
                .when('/login', {
                    templateUrl: 'partials/auth.html',
                    controller: 'AuthController'
                })
                .when('/sign_up', {
                    templateUrl: 'partials/sign_up.html',
                    controller: 'AuthController'
                })
                .otherwise({
                    redirectTo: '/fields'
                });
        }]);