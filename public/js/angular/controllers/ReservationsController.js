/*global angular,moment, console*/
(function () {
    "use strict";
    angular.module('app.reservations', [])
        .controller('ReservationsController', function ($scope, $rootScope, $routeParams, Reservation) {
            var self = this;
            $scope.field = {};
            $scope.reserved = [];
            $scope.startDate = moment();
            $scope.endDate = moment().add(5, 'days');
            $scope.days = [1, 2, 3, 4, 5];
            $scope.dates = [];
            $scope.hours = [];
            angular.forEach($scope.days, function (key, value) {
                $scope.dates.push(moment().add(value, 'days').format('D-M-YYYY'));
            });
            self.getReservation = function () {
                Reservation.get($routeParams.field_id)
                    .success(function (data) {
                        $scope.field = data;

                        var from = moment($scope.field.company.opens_at, 'H:m:s'),
                            to = moment($scope.field.company.closes_at, 'H:m:s'),
                            minutes = $scope.field.company.time_split,
                            time;
                        $scope.hours = [];
                        for (time = from; time < to; time = time.add(minutes, 'm')) {
                            $scope.hours.push(time.format('H:m'));
                        }
                    });
            };
            this.getReservation();

            $scope.addReserved = function (date, hour) {
                var dateTime = moment(date + " " + hour, 'D-M-YYYY H:m').format('D-M-YYYY H:m');
                if ($scope.reserved.indexOf(dateTime) < 0) {
                    $scope.reserved.push(dateTime);
                }
            };
            $scope.removeReserved = function (dateTime) {
                var index = $scope.reserved.indexOf(dateTime);
                if (index >= 0) {
                    $scope.reserved.splice(index, 1);
                }
            };

            $scope.store = function () {
                Reservation.store($scope.field.id, $scope.reserved)
                    .then(function (response) {
                        response = angular.fromJson(response);
                        $rootScope.messages = [];
                        $rootScope.messages.push(response.data);
                        self.getReservation();
                        $scope.reserved = [];

                    }, function (response) {
                        console.log(response);
                    });
            };

            $scope.isReserved = function (date, hour) {
                var dateTime = moment(date + " " + hour, 'D-M-YYYY H:m').format('YYYY-M-D H:m'),
                    classes = "td td-cell bg-gray-lighter",
                    contains = false;
                angular.forEach($scope.field.reservations, function (reservation) {
                    var startsAt = moment(reservation.starts_at).format('YYYY-M-D H:m');
                    if (startsAt === dateTime) {
                        contains = true;
                    }
                });
                if (contains) {
                    classes = "td td-cell background-warning";
                }
                return classes;
            };
        });
}());
