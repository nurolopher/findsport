/**
 * @ngdoc controller
 * @name Login
 *
 * @description
 * _Please update the description and dependencies._
 *
 * @requires $scope
 * */
(function () {
    "use strict";
    angular.module('app.auth', [])
        .controller('AuthController', function ($scope, $auth, $location, $window) {
            $scope.error = false;
            $scope.auth = {
                email: "",
                password: ''
            };
            $scope.login = function () {
                $auth.login($scope.auth).then(function (response) {
                    $location.path('/');
                    $window.location.reload();
                })
                    .catch(function (response) {
                        $scope.error = true;
                    });
            };

            $scope.signUp = function () {
                $auth.signup($scope.auth).then(function (response) {
                    console.log(response);
                });
            }
        });
}());
