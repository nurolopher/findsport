/**
 * Created by nursultan on 27-Jun 15.
 */
/*jslint browser: true*/
/*global $, angular*/
(function () {
    'use strict';
    angular.module('app.fields', [])
        .controller('FieldsController', function ($scope, $http, Field, Cover, Infrastructure, Review, Category) {

            $scope.loading = true;
            $scope.roof = 2;
            $scope.fields = [];
            $scope.orderByProperty = 'name';
            $scope.moreFilters = true;
            $scope.priceSlider = {
                min: 200,
                max: 2500,
                floor: 100
            };
            $scope.cover = {
                covers: [],
                selectedCovers: [],
                allIds: [],
                checkAll: function () {
                    this.selectedCovers = this.allIds.slice();
                },
                uncheckAll: function () {
                    this.selectedCovers = [];
                }
            };
            $scope.infrastructure = {
                infrastructures: [],
                allIds: [],
                selected: [],
                checkAll: function () {
                    this.selected = this.allIds.slice();
                },
                uncheckAll: function () {
                    this.selected = [];
                }
            };

            $scope.review = {
                statistics: []
            };

            $scope.fields.maxPrice = function (data) {
                var localMax = 2500, i;
                for (i = 0; i < data.length; i += 1) {
                    if (data[i].price > localMax) {
                        localMax = data[i].price;
                    }
                }
                $scope.priceSlider.max = parseInt(localMax, 10);
            };
            $scope.category = {
                categories: [],
                selected: []
            };
            Field.get()
                .success(function (data) {
                    $scope.fields = data;
                    $scope.loading = false;
                });
            Cover.get()
                .success(function (data) {
                    $scope.cover.covers = data;
                    var i;
                    Cover.getSelectedIds = function () {
                        for (i = 0; i < $scope.cover.covers.length; i += 1) {
                            $scope.cover.selectedCovers[i] = $scope.cover.covers[i].id;
                        }
                        $scope.cover.allIds = $scope.cover.selectedCovers.slice();
                    };
                    Cover.getSelectedIds();
                });

            Infrastructure.get()
                .success(function (data) {
                    $scope.infrastructure.infrastructures = data;
                    var i;
                    Infrastructure.getSelectedIds = function () {
                        for (i = 0; i < $scope.infrastructure.infrastructures.length; i += 1) {
                            $scope.infrastructure.selected[i] = $scope.infrastructure.infrastructures[i].id;
                        }
                        $scope.infrastructure.allIds = $scope.infrastructure.selected.slice();
                    };
                    Infrastructure.getSelectedIds();
                });
            Review.statistics()
                .success(function (data) {
                    var maxId = 0, index;
                    for (index = 0; index < data.length; index += 1) {
                        if (data[index]['field_id'] > maxId) {
                            maxId = data[index]['id'];
                        }
                    }
                    $scope.review.statistics = new Array(maxId);
                    for (index = 0; index < data.length; index += 1) {
                        $scope.review.statistics[data[index]['field_id']] = data[index];
                    }
                });
            Category.get()
                .success(function (data) {
                    var index;
                    $scope.category.categories = data;
                    for (index = 0; index < data.length; index += 1) {
                        $scope.category.selected[index] = data[index].id;
                    }
                });
        });
}());