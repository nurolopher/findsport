/**
 * @ngdoc controller
 * @name user
 *
 * @description
 * _Please update the description and dependencies._
 *
 * @requires $scope
 * */
angular.module('app.user', [])
    .controller('userCtrl', function ($scope, $auth) {
        $scope.authenticated = $auth.isAuthenticated();
        $scope.logout = function () {
            $auth.logout();
        };
    });
