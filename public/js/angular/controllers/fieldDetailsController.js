(function () {
    "use strict";
    angular.module('app.fieldDetails', [])
        .controller('FieldDetailsController', function ($scope, $http, $routeParams, Field) {
            $scope.field = {};
            $scope.reviewCount = 0;
            $scope.totalAvg = 0;
            $scope.loading = true;
            $scope.reviews = [];
            $scope.id = $routeParams.field_id;
            Field.show($scope.id)
                .success(function (data) {
                    $scope.field = data[0];
                    $scope.totalAvg = ((Number($scope.field.average['location']) + Number($scope.field.average['service']) + Number($scope.field.average['condition'])) / 3).toFixed(2);

                    /*Average Review calculation*/
                    $scope.reviews = data[0]['reviews'];
                    $scope.reviewCount = data[0]['reviews'].length;
                });
        });

}());