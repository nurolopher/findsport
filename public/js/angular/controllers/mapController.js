/**
 * Created by nurolopher on 7/26/2015.
 */
(function () {
    'use strict';
    angular.module('app.map', ['uiGmapgoogle-maps'])
        .config(function (uiGmapGoogleMapApiProvider) {
            uiGmapGoogleMapApiProvider.configure({
                key: 'AIzaSyCmXz4l5dv0yncwJdwoNlPw6UdK4toiNU4',
                v: '3.17',
                libraries: 'weather,geometry,visualization'
            });
        })
        .controller('MapController', function ($scope, $http, $routeParams, Map, Field) {
            $scope.id = $routeParams.field_id;
            $scope.field = {};
            $scope.fields = {};
            Map.show($scope.id)
                .success(function (data) {
                    $scope.field = data;
                    jQuery(window).scrollTop();
                });
            Field.get()
                .success(function (data) {
                    $scope.fields = data;
                })
        });
}());