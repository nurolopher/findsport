/*jslint browser: true*/
/*global angular*/
(function () {
    'use strict';
    var app = angular.module('app',
        [
            'rzModule',
            'checklist-model',
            'ngRoute',
            'ui.bootstrap',
            'uiGmapgoogle-maps',
            'satellizer',
            'ngSanitize',
            'pascalprecht.translate',
            'app.fieldService',
            'app.map',
            'app.fields',
            'app.fieldDetails',
            'app.reservations',
            'app.auth',
            'app.user'
        ]);

    app.config(function (uiGmapGoogleMapApiProvider) {
        uiGmapGoogleMapApiProvider.configure({
            key: 'AIzaSyCmXz4l5dv0yncwJdwoNlPw6UdK4toiNU4',
            v: '3.17',
            libraries: ''
        });
    });

    app.config(function ($authProvider) {
        $authProvider.loginUrl = '/api/login';
        $authProvider.signupUrl = '/api/signUp';
        $authProvider.facebook({
            clientId: '915541218507125'
        });
    });
}());
